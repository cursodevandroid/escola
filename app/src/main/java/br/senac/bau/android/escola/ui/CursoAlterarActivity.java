package br.senac.bau.android.escola.ui;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import br.senac.bau.android.escola.R;
import br.senac.bau.android.escola.modelo.Curso;
import br.senac.bau.android.escola.modelo.dao.CursoDAO;

public class CursoAlterarActivity extends AppCompatActivity {

    private Curso curso;
    private CursoDAO dao;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_curso_alterar);

        dao = new CursoDAO(getBaseContext());

        long id = getIntent().getLongExtra("id", 0);
        curso = dao.consultar(id);


        Button btSalvar = findViewById(R.id.btSalvar);
        final EditText etNome = findViewById(R.id.etNome);

        etNome.setText(curso.getNome());

        btSalvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                curso.setNome(etNome.getText().toString());

                try {
                    dao.alterar(curso);
                    setResult(RESULT_OK);
                    finish();
                } catch (Exception e) {
                    Toast.makeText(getBaseContext(),
                            e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        });


    }
}
