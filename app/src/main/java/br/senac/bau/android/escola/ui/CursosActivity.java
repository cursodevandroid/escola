package br.senac.bau.android.escola.ui;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import br.senac.bau.android.escola.R;
import br.senac.bau.android.escola.modelo.Curso;
import br.senac.bau.android.escola.modelo.dao.CursoDAO;

import static br.senac.bau.android.escola.R.*;

public class CursosActivity extends AppCompatActivity {
    private List<Curso> cursos;
    private ListView listView;
    private CursoDAO dao;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(layout.activity_cursos);
        listView = findViewById(id.listview);

        dao = new CursoDAO(getBaseContext());

        cursos = dao.consultarTodos();

        listView.setAdapter(new BaseAdapter() {
            @Override
            public int getCount() {
                return CursosActivity.this.cursos.size();
            }

            @Override
            public Object getItem(int position) {
                Curso curso = CursosActivity.this.cursos.get(position);
                return curso;
            }

            @Override
            public long getItemId(int position) {
                Curso curso = CursosActivity.this.cursos.get(position);
                return curso.getId();
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = LayoutInflater.from(getBaseContext())
                        .inflate(layout.item_curso, parent, false);
                TextView tvId = view.findViewById(id.tvId);
                TextView tvNome = view.findViewById(id.tvNome);

                Curso curso = CursosActivity.this.cursos.get(position);

                tvId.setText(Long.toString(curso.getId()));
                tvNome.setText(curso.getNome());

                return view;
            }
        });

        registerForContextMenu(listView);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                this.cursos = dao.consultarTodos();
                listView.invalidateViews();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.curso_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.adicionar:
                Intent intent = new Intent(getBaseContext(),
                        CursoAdicionarActivity.class);

                startActivityForResult(intent, 1);

                return true;

            case R.id.teste:
                //programar ação
                Toast.makeText(this, "Clicou no teste",
                        Toast.LENGTH_LONG).show();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.curso_lista_menu, menu);

    }

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {

        AdapterView.AdapterContextMenuInfo info =
                (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        Curso curso = cursos.get(info.position);

        switch (item.getItemId()) {
            case R.id.alterar:
                Intent intent = new Intent(CursosActivity.this,
                        CursoAlterarActivity.class);
                intent.putExtra("id", curso.getId());
                startActivityForResult(intent, 1);
                return true;
            case R.id.excluir:
                try {
                    dao.excluir(curso);
                    cursos = dao.consultarTodos();
                    listView.invalidateViews();
                } catch (Exception e) {
                    Toast.makeText(getBaseContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                }
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }
}
