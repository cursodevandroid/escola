package br.senac.bau.android.escola.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import br.senac.bau.android.escola.R;
import br.senac.bau.android.escola.modelo.Curso;
import br.senac.bau.android.escola.modelo.dao.CursoDAO;

public class CursoAdicionarActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_curso_adicionar);

        Button btSalvar = findViewById(R.id.btSalvar);
        final EditText etNome = findViewById(R.id.etNome);


        btSalvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CursoDAO dao = new CursoDAO(getBaseContext());

                Curso curso = new Curso();
                curso.setNome(etNome.getText().toString());

                try {
                    dao.inserir(curso);
                    setResult(RESULT_OK);
                    finish();
                } catch (Exception e) {
                    Toast.makeText(getBaseContext(),
                            e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }
        });


    }
}
