package br.senac.bau.android.escola.modelo.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import br.senac.bau.android.escola.modelo.Curso;

public class CursoDAO {
    private CriarBanco banco;

    public CursoDAO(Context context) {
        this.banco = new CriarBanco(context);
    }

    public void inserir(Curso curso) throws Exception {

        SQLiteDatabase db = this.banco.getWritableDatabase();

        ContentValues valores = new ContentValues();
        valores.put("nome", curso.getNome());

        long result = db.insert("cursos", null, valores);
        db.close();

        if (result == -1)
            throw new Exception("Registro não inserido");


        curso.setId(result);
    }

    public void alterar(Curso curso) throws Exception {
        SQLiteDatabase db = this.banco.getWritableDatabase();

        ContentValues valores = new ContentValues();
        valores.put("nome", curso.getNome());

        int resultado = db.update("cursos",
                valores, "_id = " + curso.getId(), null);

        db.close();

        if (resultado == -1)
            throw new Exception("Erro ao atualizar registro");

    }

    public void excluir(Curso curso) throws Exception {
        SQLiteDatabase db = this.banco.getWritableDatabase();

        int resultado = db.delete("cursos",
                "_id = " + curso.getId(),null);

        if (resultado == -1)
            throw new Exception("Erro ao remover registro");

    }

    public List<Curso> consultarTodos() {
        String [] campos = {"_id", "nome"};
        SQLiteDatabase db = this.banco.getReadableDatabase();
        Cursor cursor = db.query("cursos", campos, null,
                null, null, null, null);
        List<Curso> cursos = new ArrayList<>();
        while (cursor.moveToNext()) {
            Curso curso = new Curso();
            curso.setId(cursor.getLong(
                cursor.getColumnIndexOrThrow("_id")
            ));
            curso.setNome(cursor.getString(
                    cursor.getColumnIndexOrThrow("nome")
            ));
            cursos.add(curso);
        }
        db.close();

        return cursos;
    }

    public Curso consultar(long id) {

        String [] campos = {"_id", "nome"};
        String [] args = {Long.toString(id)};
        SQLiteDatabase db = this.banco.getReadableDatabase();
        Cursor cursor = db.query("cursos", campos, "_id = ?",
                args, null, null, null);
        Curso curso = null;
        if (cursor.moveToNext()) {
            curso = new Curso();
            curso.setId(cursor.getLong(
                    cursor.getColumnIndexOrThrow("_id")
            ));
            curso.setNome(cursor.getString(
                    cursor.getColumnIndexOrThrow("nome")
            ));
        }
        db.close();

        return curso;
    }
}
