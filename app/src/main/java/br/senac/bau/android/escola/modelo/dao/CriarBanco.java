package br.senac.bau.android.escola.modelo.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class CriarBanco extends SQLiteOpenHelper {

    public CriarBanco(Context context) {
        super(context, "Escola.db", null, 1);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {
        String tabelaCurso = "CREATE TABLE cursos (" +
                "_id integer primary key autoincrement, " +
                "nome text not null" +
                ")";

        db.execSQL(tabelaCurso);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE cursos");
        onCreate(db);
    }
}
