package br.senac.bau.android.escola;

import android.content.Context;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.platform.app.InstrumentationRegistry;

import org.junit.Test;
import org.junit.runner.RunWith;

import br.senac.bau.android.escola.modelo.Curso;
import br.senac.bau.android.escola.modelo.dao.CursoDAO;

import static org.junit.Assert.assertEquals;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class CursoDAOInstrumentedTest {
    private CursoDAO getDAO() {
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        return new CursoDAO(appContext);
    }

    @Test
    public void useAppContext() {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();

        assertEquals("br.senac.bau.android.escola", appContext.getPackageName());
    }

    @Test
    public void inserirCurso() {
        CursoDAO dao = getDAO();

        Curso curso = new Curso();
        curso.setNome("Teste");


        try {
            dao.inserir(curso);

            assertEquals(curso.getId(), 1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void consultarCurso() {
        CursoDAO dao = getDAO();

        Curso curso = new Curso();
        curso.setNome("Teste");


        try {
            dao.inserir(curso);

            //assertEquals(curso.getId(), 1);
        } catch (Exception e) {
            e.printStackTrace();
        }


        Curso curso1 = dao.consultar(1);

    }



}
